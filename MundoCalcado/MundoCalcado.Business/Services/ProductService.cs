﻿using Microsoft.EntityFrameworkCore;
using MundoCalcado.Business.Interfaces;
using MundoCalcado.Domain;
using MundoCalcado.Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MundoCalcado.Business.Services
{
    public class ProductService : IProductService
    {
        private readonly MundoCalcadoDbContext context;

        public ProductService(MundoCalcadoDbContext context)
        {
            this.context = context;
        }

        public async Task<List<Product>> GetProducts()
        {
            return await context.Product.Take(8).ToListAsync();
        }

        public async Task<Product> GetProduct(int id)
        {
            return await context.Product.FirstOrDefaultAsync(x => x.IdProduct == id);
        }

        public async Task<List<Product>> GetRelatedProducts(int id)
        {
            var product = await GetProduct(id);

            if (product == null)
                return null;

            // Return products with the same category, genre and type
            var result = context.Product
                .Where(m =>
                    m.IdProduct != product.IdProduct &&
                    m.Genre == product.Genre &&
                    (m.IdCategory == product.IdCategory ||
                    m.Type == product.Type))
                .Take(4)
                .ToListAsync();

            return await result;
        }

        public async Task<IEnumerable<string>> GetGenres()
        {
            var genres = new List<string>();

            genres.Add("Homem");
            genres.Add("Mulher");
            genres.Add("Unisexo");

            return genres;
        }

        public async Task<IEnumerable<Category>> GetCategories()
        {
            return await context.Category.ToListAsync();
        }

        public async Task<Category> GetCategory(int id)
        {
            return await context.Category.FirstOrDefaultAsync(m => m.IdCategory == id);
        }

        public async Task<IEnumerable<Color>> GetColors()
        {
            return await context.Color.ToListAsync();
        }

        public async Task<IEnumerable<Color>> GetAvailableColors(int id)
        {           
            
            return await context.ProdColor.Where(m => m.IdProduct == id).Select(m => m.Color).ToListAsync();
        }

        public async Task<IEnumerable<string>> GetTypes()
        {
            var types = new List<string>();

            types.Add("Formal");
            types.Add("Informal");

            return types;
        }

        public async Task<IEnumerable<string>> GetBrands()
        {
            var brands = new List<string>();

            brands.Add("Adidas");
            brands.Add("Nike");
            brands.Add("Tapadas");
            brands.Add("New Balance");
            brands.Add("All Starts");
            brands.Add("Vans");

            return brands;
        }
    }
}
