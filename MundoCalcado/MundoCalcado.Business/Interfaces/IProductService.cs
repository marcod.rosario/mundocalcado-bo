﻿using MundoCalcado.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MundoCalcado.Business.Interfaces
{
    public interface IProductService
    {
        Task<List<Product>> GetProducts();

        Task<Product> GetProduct(int id);

        Task<List<Product>> GetRelatedProducts(int id);

        Task<IEnumerable<string>> GetGenres();

        Task<IEnumerable<Category>> GetCategories();

        Task<Category> GetCategory(int id);

        Task<IEnumerable<Color>> GetAvailableColors(int id);

        Task<IEnumerable<Color>> GetColors();

        Task<IEnumerable<string>> GetTypes();

        Task<IEnumerable<string>> GetBrands();
    }
}
