﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MundoCalcadoApi.Models
{
    public class ProductItemModel
    {
        public ProductItemModel()
        {
        }

        public ProductItemModel (
            int id,
            string name,
            string category,
            decimal price,
            string imageUrl,
            string description,
            string availibility,
            string genre,
            string composition,
            string type,
            string qualityCheck
            ) 
        {
            Id = id;
            Name = name;
            Category = category;
            Price = price;
            ImageUrl = imageUrl;
            Description = description;
            Availibility = availibility;
            Genre = genre;
            Composition = composition;
            Type = type;
            QualityCheck = qualityCheck;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public decimal Price { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }
        public string Availibility { get; set; }
        public string Genre { get; set; }
        public string Composition { get; set; }
        public string Type { get; set; }
        public string QualityCheck { get; set; }

    }
}
