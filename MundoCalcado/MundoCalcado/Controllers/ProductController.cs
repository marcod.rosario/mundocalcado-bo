﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MundoCalcado.Business.Interfaces;
using MundoCalcado.Domain.Entities;
using MundoCalcadoApi.Models;

namespace MundoCalcadoApi.Controllers
{
    public class ProductController : BaseController
    {
        private readonly IProductService service;

        public ProductController(IProductService service)
        { 
            this.service = service;
        }

        // GET: api/Product/GetMostSelled
        [HttpGet]
        public async Task<List<ProductItemModel>> GetMostSelled()
        {
            var products = await service.GetProducts();

            if (!products.Any())
            {
                return null;
            }

            return MapProductsToResponse(products);
        }

        // GET: api/Product/GetTrends
        [HttpGet]
        public async Task<List<ProductItemModel>> GetTrends()
        {
            var products = await service.GetProducts();

            if(!products.Any())
            {
                return null;
            }

            return MapProductsToResponse(products);
        }

        // GET: api/Product/GetGenres
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<string>), StatusCodes.Status200OK)]
        public async Task<IEnumerable<string>> GetGenres()
        {
            var genres = await service.GetGenres();

            if (!genres.Any())
            {
                return null;
            }

            return genres;
        }

        // GET: api/Product/GetProduct/5
        [HttpGet("{id}")]
        public async Task<ProductItemModel> GetProduct(int id)
        {
            var product = await service.GetProduct(id);

            if (product == null)
            {
                return null;
            }

            return MapProductToResponse(product);
        }

        // GET: api/Product/GetRelatedProducts
        [HttpGet("{id}")]
        public async Task<List<ProductItemModel>> GetRelatedProducts(int id)
        {
            var products = await service.GetRelatedProducts(id);

            if (!products.Any())
            {
                return null;
            }

            return MapProductsToResponse(products);
        }

        // GET: api/Product/GetCategories
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Category>), StatusCodes.Status200OK)]
        public async Task<IEnumerable<Category>> GetCategories()
        {
            var categories = await service.GetCategories();

            if (!categories.Any())
            {
                return null;
            }

            return categories;
        }

        // GET: api/Product/GetAvailableColors
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(IEnumerable<Color>), StatusCodes.Status200OK)]
        public async Task<IEnumerable<string>> GetAvailableColors(int id)
        {
            var colors = await service.GetAvailableColors(id);

            if (!colors.Any())
                return null;
            
            var result = new List<string>();
            foreach (var color in colors)
            {
                result.Add(color.Name);
            }

            return result;
        }

        // GET: api/Product/GetProductGenres
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<string>), StatusCodes.Status200OK)]
        public async Task<IEnumerable<string>> GetTypes()
        {
            var types = await service.GetTypes();

            if (!types.Any())
            {
                return null;
            }

            return types;
        }

        // GET: api/Product/GetBrands
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<string>), StatusCodes.Status200OK)]
        public async Task<IEnumerable<string>> GetBrands()
        {
            var brands = await service.GetBrands();

            if (!brands.Any())
            {
                return null;
            }

            return brands;
        }

        // PUT: api/Product/5
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutProduct(int id, Product product)
        //{
        //    if (id != product.IdProduct)
        //    {
        //        return BadRequest();
        //    }

        //    service.Entry(product).State = EntityState.Modified;

        //    try
        //    {
        //        await service.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!ProductExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        // POST: api/Product
        //[HttpPost]
        //public async Task<ActionResult<Product>> PostProduct(Product product)
        //{

        //    service.Product.Add(product);
        //    try
        //    {
        //        await service.SaveChangesAsync();
        //    }
        //    catch (DbUpdateException)
        //    {
        //        if (ProductExists(product.IdProduct))
        //        {
        //            return Conflict();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return CreatedAtAction("GetProduct", new { id = product.IdProduct }, product);
        //}

        // DELETE: api/Product/5
        //[HttpDelete("{id}")]
        //public async Task<ActionResult<Product>> DeleteProduct(int id)
        //{
        //    var product = await service.Product.FindAsync(id);
        //    if (product == null)
        //    {
        //        return NotFound();
        //    }

        //    service.Product.Remove(product);
        //    await service.SaveChangesAsync();

        //    return product;
        //}

        //private bool ProductExists(int id)
        //{
        //    return service.Product.Any(e => e.IdProduct == id);
        //}

        #region Private Methods

        private List<ProductItemModel> MapProductsToResponse(List<Product> products)
        {
            var items = new List<ProductItemModel>();
            foreach (var product in products)
            {
                items.Add(MapProductToResponse(product));
            }

            return items;
        }

        private ProductItemModel MapProductToResponse(Product product)
        {
            var categoryName = service.GetCategory(product.IdCategory).Result.Name;

            return new ProductItemModel(
                       id: product.IdProduct,
                       name: product.Name,
                       category: categoryName,
                       price: product.Price,
                       imageUrl: product.ImageUrl,
                       description: product.Description,
                       availibility: product.Availibility ? "Sim" : "Não",
                       genre: product.Genre,
                       composition: product.Composition,
                       type: product.Type,
                       qualityCheck: product.QualityCheck ? "Sim" : "Não"
                       );
        }
        #endregion
    }
}
