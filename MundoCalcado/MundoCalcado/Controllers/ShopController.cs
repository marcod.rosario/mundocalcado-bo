﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MundoCalcado.Business.Interfaces;
using MundoCalcado.Domain.Entities;
using MundoCalcadoApi.Models;

namespace MundoCalcadoApi.Controllers
{
    public class ShopController : BaseController
    {
        private readonly IProductService service;

        public ShopController(IProductService service)
        { 
            this.service = service;
        }

        // GET: api/Shop/GetMarcas
        [HttpGet]
        [ProducesResponseType(typeof(List<ShopFilterItemModel>), StatusCodes.Status200OK)]
        public async Task<List<ShopFilterItemModel>> GetFilterBrands()
        {
            var brands = await service.GetBrands();

            if (!brands.Any())
            {
                return null;
            }

            var brandFilters = new List<ShopFilterItemModel>();

            foreach (var brand in brands)
            {
                brandFilters.Add(
                    new ShopFilterItemModel(
                       name: brand,
                       count: 0 // TODO: CHANGE
                    )
                );
            }

            return brandFilters;
        }

        // GET: api/Shop/GetColors
        [HttpGet]
        [ProducesResponseType(typeof(List<ShopFilterItemModel>), StatusCodes.Status200OK)]
        public async Task<List<ShopFilterItemModel>> GetFilterColors()
        {
            var colors = await service.GetColors();

            if (!colors.Any())
            {
                return null;
            }

            var colorFilters = new List<ShopFilterItemModel>();

            foreach (var color in colors)
            {
                colorFilters.Add(
                    new ShopFilterItemModel(
                       name: color.Name,
                       count: 0 // TODO: CHANGE
                    )
                );

            }

            return colorFilters;
        }

        // GET: api/Shop/GetProductTypes
        [HttpGet]
        [ProducesResponseType(typeof(List<ShopFilterItemModel>), StatusCodes.Status200OK)]
        public async Task<List<ShopFilterItemModel>> GetFilterTypes()
        {
            var types = await service.GetCategories();

            if (!types.Any())
            {
                return null;
            }

            var typeFilters = new List<ShopFilterItemModel>();

            foreach (var type in types)
            {
                typeFilters.Add(
                    new ShopFilterItemModel(
                       name: type.Name,
                       count: 0 // TODO: CHANGE
                    )
                );

            }

            return typeFilters;
        }

    }
}
